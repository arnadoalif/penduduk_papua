<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
      <li><a><i class="fa fa-male"></i> Kependudukan <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo base_url('data_penduduk'); ?>">Data Penduduk</a></li>
          <li><a href="<?php echo base_url('data_distrik'); ?>">Data Distrik</a></li>
          <li><a href="<?php echo base_url('data_kampung'); ?>">Data Kampung</a></li>
        </ul>
      </li>
    </ul>
  </div>

  <div class="menu_section">
    <h3>Configuration</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-bug"></i> Setting <span class="fa fa-chevron-down"></span></a></li>
          
    </ul>
  </div>
</div>
<!-- /sidebar menu -->