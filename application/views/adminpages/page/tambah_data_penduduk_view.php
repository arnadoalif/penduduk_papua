<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>DATA PENDUDUK</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/admin_icon.gif'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="col-md-12 col-xs-12">
            <div class="x_panel">
              <?php if (isset($_SESSION['message_data'])): ?>
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
                  <?php echo $_SESSION['message_data'] ?>
                </div>
                <?php endif ?>

                <?php if (isset($_SESSION['error_data'])): ?>
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
                  <?php echo $_SESSION['error_data'] ?>
                </div>
              <?php endif ?>
              <div class="x_title">
                <h2>Tambah Penduduk</h2>
                <div class="clearfix"></div>
              </div>

              <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" action="<?php echo base_url('administrator/action_tambah_data_penduduk'); ?>" method="POST">

                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">NO KK</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kk" class="form-control numbers" placeholder="Nomor Kartu Keluarga">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">NO NIK</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="nik" class="form-control numbers" placeholder="Nomor NIK">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelurahan</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kelurahan" class="form-control numbers" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="nama" class="form-control alphaonly" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelamin</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select name="kelamin" class="form-control">
                          <option value="L">Laki - Laki</option>
                          <option value="P">Perempuan</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat , Tanggal Lahir</label>
                      <div class="col-md-3 col-sm-9 col-xs-12">
                        <input type="text" name="tempat" class="form-control alphaonly" placeholder="Tempat Lahir">
                      </div>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <div class="form-group">
                            <div class='input-group date' id='myDatepicker2'>
                                <input type='text' name="tanggal_lahir" class="form-control" placeholder="Format : Day/Month/Year" />
                                <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                      </div>
                        
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">SHDK</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <select name="shdk" required class="form-control">
                          <option value="KEPALA KELUARGA">KEPALA KELUARGA</option>
                          <option value="ISTRI">ISTRI</option>
                          <option value="ORANG TUA">ORANG TUA</option>
                          <option value="MERTUA">MERTUA</option>
                          <option value="ANAK">ANAK</option>
                          <option value="CUCU">CUCU</option>
                          <option value="FAMILI LAIN">FAMILI LAIN</option>
                          <option value="-" selected="">PILIH SHDK</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">SHDRT</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="shdrt" required class="form-control numbers" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Golongan Darah</label>
                      <div class="col-md-3 col-sm-9 col-xs-12">
                        <select name="golongan_darah" class="form-control">
                          <option value="A">A</option>
                          <option value="B">B</option>
                          <option value="AB">AB</option>
                          <option value="0">0</option>
                          <option value="-" selected>Tidak Tahu</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Agama</label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="agama" required class="form-control">
                          <option value="-" selected="">Pilih Agama</option>
                          <option value="ISLAM">ISLAM</option>
                          <option value="KRISTEN">KRISTEN</option>
                          <option value="KATOLIK">KATOLIK</option>
                          <option value="HINDU">HINDU</option>
                          <option value="BUDDHA">BUDDHA</option>
                          <option value="KONG HU CU">Kong Hu Cu</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Status Kawin</label>
                      <div class="col-md-4 col-sm-9 col-xs-12">
                        <select name="status" class="form-control">
                          <option value="-" selected>PILIH STATUS</option>
                          <option value="BELUM KAWIN">BELUM KAWIN</option>
                          <option value="KAWIN">KAWIN</option>
                          <option value="CERAI HIDUP">CERAI HIDUP</option>
                          <option value="CERAI MATI">CERAI MATI</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select name="pendidikan" class="form-control">
                          <option value="-" selected="">Pilih Pendidikan</option>
                          <option value="AKADEMI/DIPLOMA III/S. MUDA">AKADEMI/DIPLOMA III/S. MUDA</option>  
                          <option value="DIPLOMA I/II">DIPLOMA I/II</option>                 
                          <option value="DIPLOMA IV/STRATA I">DIPLOMA IV/STRATA I</option>          
                          <option value="SLTA/SEDERAJAT">SLTA/SEDERAJAT</option>               
                          <option value="SLTP/SEDERAJAT">SLTP/SEDERAJAT</option>               
                          <option value="STRATA II">STRATA II</option>                    
                          <option value="TAMAT SD/SEDERAJAT">TAMAT SD/SEDERAJAT</option>           
                          <option value="TIDAK TAMAT SD/SEDERAJAT">TIDAK TAMAT SD/SEDERAJAT</option>     
                          <option value="TIDAK/BELUM SEKOLAH">TIDAK/BELUM SEKOLAH</option>  
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="pekerjaan" required class="form-control alphaonly" placeholder="">
                      </div>
                    </div>
                    
                  </div>

                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ibu</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="nama_ibu" class="form-control alphaonly " placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ayah</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="nama_ayah" class="form-control alphaonly " placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kepala Keluarga</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kepala_keluarga" class="form-control alphaonly " placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Lengkap</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <textarea name="alamat" class="form-control" rows="3" required="required"></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Propinsi</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="prop" class="form-control alphaonly" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kabupaten</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kab" class="form-control alphaonly" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kecamatan</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kec" class="form-control alphaonly" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Kampung</label>
                      <div class="col-md-6 col-sm-9 col-xs-12">
                        <input type="text" name="kampung" class="form-control" placeholder="">
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <button type="button" class="btn btn-danger">Cancel</button>
                        <button type="reset" class="btn btn-primary"><i class="fa fa-refresh"></i> Reset</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Penduduk</button>
                      </div>
                    </div>

                  </div>

                </form>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
  </body>
</html>