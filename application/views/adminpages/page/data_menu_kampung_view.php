<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>DATA PENDUDUK</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('asset_admin/images/admin_icon.gif'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="x_panel">
            <div class="x_title">
              <div class="col-md-6 col-lg-6">
                <h2>Data Kampung </h2>
              </div>
              <div class="col-md-6 col-lg-3 pull-right">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top: 8px;"><i class="fa fa-filter"></i> Kampung</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="data_kampung" id="pilih_kampung" class="form-control" required="required">
                      <option value="" selected>-- PILIH KAMPUNG --</option>
                      <?php foreach ($data_kampung as $dt_kecamatan): ?>
                        <option value="<?php echo $dt_kecamatan->kampung ?>"><?php echo $dt_kecamatan->kampung ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
              </div>
              
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                      
                    </p>

                    <div id="view_data"></div>
                    <div id="loading" style="position: relative;margin: 0 auto;text-align: center;">
                      <img width="80" height="80" id="loading_image"   src="<?php echo base_url('asset_admin/images/loading.gif'); ?>" alt="processing...">
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>
    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
        var nama_kampung = "";
        $('#loading').hide();
        $('#pilih_kampung').change(function(event) {
           nama_kampung = $(this).val();
           get_data(nama_kampung);
           $("#view_data").hide();
           $('#loading').show();
        });

      });

      function get_data(data_2) {
        // console.log("OKE data "+data_1+" "+ data_2);

        $.ajax({
          url: '<?php echo base_url('administrator/get_data_filter_kampung'); ?>',
          type: 'POST',
          data: {data_2: data_2},
        })
        .done(function(e) {
          $("#view_data").show();
          $("#view_data").html(e);
          $('#loading').hide();
          init_DataTables();
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
        
      }


      
    </script>
  </body>
</html>