<div class="table-responsive">
  <table id="datatable-buttons" class="table table-hover">
    <thead>
      <tr>
        <th>KK</th>
        <th>NIK</th>
        <th>NAMA</th>
        <th>KELAMIN</th>
        <th>DARAH</th>
        <th>AGAMA</th>
        <th>STATUS</th>
        <th>PEKERJAAN</th>
        <th>PROP</th>
        <th>KAB</th>
        <th>KEC</th>
        <th>KAMPUNG</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($data_penduduk as $dt_penduduk): ?>
        <tr>
          <td><?php echo $dt_penduduk->kk ?></td>
          <td><?php echo $dt_penduduk->nik ?></td>
          <td><?php echo $dt_penduduk->nama ?></td>
          <td><?php echo $dt_penduduk->kelamin ?></td>
          <td><?php echo $dt_penduduk->darah ?></td>
          <td><?php echo $dt_penduduk->agama ?></td>
          <td><?php echo $dt_penduduk->status ?></td>
          <td><?php echo $dt_penduduk->pekerjaan ?></td>
          <td><?php echo $dt_penduduk->prop ?></td>
          <td><?php echo $dt_penduduk->kab ?></td>
          <td><?php echo $dt_penduduk->kec ?></td>
          <td><?php echo $dt_penduduk->kampung ?></td>
          <td>
            <a class="btn btn-info btn-xs" href="<?php echo base_url('edit_penduduk/'.$dt_penduduk->kk.'/'.$dt_penduduk->nik); ?>" role="button"><i class="fa fa-pencil-square"></i></a>
            <a class="btn btn-warning btn-xs" href="#" role="button"><i class="fa fa-eye"></i></a>
            <a class="btn btn-danger btn-xs delete" data-kk="<?php echo $dt_penduduk->kk ?>" data-nik="<?php echo $dt_penduduk->nik ?>" role="button"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
      <?php endforeach ?>
      
    </tbody>
  </table>
</div>