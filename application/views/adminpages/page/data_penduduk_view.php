<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/admin_style.php'); ?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('admin/home') ?>" class="site_title"><span>DATA PENDUDUK</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
               <img src="<?php echo base_url('asset_admin/images/admin_icon.gif'); ?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Administrator</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <?php $this->load->view('adminpages/menu_bar'); ?>

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php $this->load->view('adminpages/nav_menu'); ?>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <a class="btn btn-info" href="<?php echo base_url('tambah_penduduk'); ?>" role="button"><i class="fa fa-plus"></i> Tambah Penduduk</a>

          <div class="table-responsive">
            <table id="datatable-buttons" class="table table-hover">
              <thead>
                <tr>
                  <th>KK</th>
                  <th>NIK</th>
                  <th>NAMA</th>
                  <th>KELAMIN</th>
                  <th>DARAH</th>
                  <th>AGAMA</th>
                  <th>STATUS</th>
                  <th>PEKERJAAN</th>
                  <th>PROP</th>
                  <th>KAB</th>
                  <th>KEC</th>
                  <th>KAMPUNG</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_penduduk as $dt_penduduk): ?>
                  <tr>
                    <td><?php echo $dt_penduduk->kk ?></td>
                    <td><?php echo $dt_penduduk->nik ?></td>
                    <td><?php echo $dt_penduduk->nama ?></td>
                    <td><?php echo $dt_penduduk->kelamin ?></td>
                    <td><?php echo $dt_penduduk->darah ?></td>
                    <td><?php echo $dt_penduduk->agama ?></td>
                    <td><?php echo $dt_penduduk->status ?></td>
                    <td><?php echo $dt_penduduk->pekerjaan ?></td>
                    <td><?php echo $dt_penduduk->prop ?></td>
                    <td><?php echo $dt_penduduk->kab ?></td>
                    <td><?php echo $dt_penduduk->kec ?></td>
                    <td><?php echo $dt_penduduk->kampung ?></td>
                    <td>
                      <a class="btn btn-info btn-xs" href="<?php echo base_url('edit_penduduk/'.$dt_penduduk->kk.'/'.$dt_penduduk->nik); ?>" role="button"><i class="fa fa-pencil-square"></i></a>
                      <a class="btn btn-warning btn-xs" href="#" role="button"><i class="fa fa-eye"></i></a>
                      <a class="btn btn-danger btn-xs delete" data-kk="<?php echo $dt_penduduk->kk ?>" data-nik="<?php echo $dt_penduduk->nik ?>" role="button"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach ?>
                
              </tbody>
            </table>
          </div>
          
        </div>
        <!-- /page content -->

        <?php $this->load->view('adminpages/footer'); ?>
      </div>
    </div>

    <?php require_once(APPPATH .'views/include/admin/admin_script.php'); ?>
    
    <script type="text/javascript">
      $(document).ready(function() {

        $(document).on('click', '.delete', function(event) {
          event.preventDefault();
          var kk = $(this).data('kk');
          var nik = $(this).data('nik');
          data_delete(kk, nik);
          // console.log(kk + " - "+ nik);
        });
        
      });
      

      function data_delete(kk, nik) {
        $.ajax({
          url: '<?php echo base_url(); ?>administrator/action_delete',
          type: 'POST',
          data: {kk: kk, nik:nik},
        })
        .done(function(e) {
          location.reload();
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
        
      }
    </script>

  </body>
</html>