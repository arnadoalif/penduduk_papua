<!-- jQuery -->
<script src="<?php echo base_url('asset_admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('asset_admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('asset_admin/vendors/fastclick/lib/fastclick.js'); ?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('asset_admin/vendors/nprogress/nprogress.js'); ?>"></script>
<!-- Chart.js -->
<script src="<?php echo base_url('asset_admin/vendors/Chart.js/dist/Chart.min.js'); ?>"></script>
<!-- jQuery Sparklines -->
<script src="<?php echo base_url('asset_admin/vendors/jquery-sparkline/dist/jquery.sparkline.min.js'); ?>"></script>

<!-- Switchery -->
<script src="<?php echo base_url('asset_admin/vendors/switchery/dist/switchery.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('asset_admin/vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- Parsley -->
<script src="<?php echo base_url('asset_admin/vendors/parsleyjs/dist/parsley.min.js'); ?>"></script>
<!-- Autosize -->
<script src="<?php echo base_url('asset_admin/vendors/autosize/dist/autosize.min.js'); ?>"></script>
<!-- jQuery autocomplete -->
<script src="<?php echo base_url('asset_admin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>
<!-- starrr -->
<script src="<?php echo base_url('asset_admin/vendors/starrr/dist/starrr.js'); ?>"></script>

<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('asset_admin/vendors/moment/min/moment.min.js'); ?> "></script>
<script src="<?php echo base_url('asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?> "></script>
<!-- bootstrap-datetimepicker -->    
<script src="<?php echo base_url('asset_admin/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?> "></script>
<!-- Ion.RangeSlider -->
<script src="<?php echo base_url('asset_admin/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js'); ?> "></script>
<!-- Bootstrap Colorpicker -->
<script src="<?php echo base_url('asset_admin/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js'); ?> "></script>
<!-- jquery.inputmask -->
<script src="<?php echo base_url('asset_admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'); ?> "></script>

<!-- Flot -->
<script src="<?php echo base_url('asset_admin/vendors/Flot/jquery.flot.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/Flot/jquery.flot.pie.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/Flot/jquery.flot.time.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/Flot/jquery.flot.stack.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/Flot/jquery.flot.resize.js'); ?>"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url('asset_admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/flot.curvedlines/curvedLines.js'); ?>"></script>
<!-- DateJS -->
<script src="<?php echo base_url('asset_admin/vendors/DateJS/build/date.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('asset_admin/vendors/moment/min/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

<!-- Datatables -->
<script src="<?php echo base_url('asset_admin/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/jszip/dist/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
<script src="<?php echo base_url('asset_admin/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url('asset_admin/vendors/summernote/dist/summernote.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset_admin/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.js'); ?>"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('asset_admin/build/js/custom.min.js'); ?>"></script>

<!-- <script src="//cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->


<script type="text/javascript">
	$('#myDatepicker2').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $(".numbers").keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	return false;
	}
	});

	$('.alphaonly').keydown(function(e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
</script>
