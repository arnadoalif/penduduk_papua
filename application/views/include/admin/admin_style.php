<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo base_url('asset_front/images/logo_sari_rosa_asih.png') ?>">
<title>Administrator</title>



<!-- Bootstrap -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo base_url('asset_admin/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url('asset_admin/vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">

<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="<?php echo base_url('asset_admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'); ?>" rel="stylesheet">

<!-- bootstrap-wysiwyg -->
<link href="<?php echo base_url('asset_admin/vendors/google-code-prettify/bin/prettify.min.css'); ?>" rel="stylesheet">
<!-- Select2 -->
<link href="<?php echo base_url('asset_admin/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
<!-- Switchery -->
<link href="<?php echo base_url('asset_admin/vendors/switchery/dist/switchery.min.css'); ?>" rel="stylesheet">
<!-- starrr -->
<link href="<?php echo base_url('asset_admin/vendors/starrr/dist/starrr.css'); ?>" rel="stylesheet">


<!-- iCheck -->
<link href="<?php echo base_url('asset_admin/vendors/iCheck/skins/flat/green.css') ?>" rel="stylesheet">
<!-- Datatables -->
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('asset_admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url('asset_admin/vendors/summernote/dist/summernote.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('asset_admin/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>">


<!-- Custom Theme Style -->
<link href="<?php echo base_url('asset_admin/build/css/custom.min.css'); ?>" rel="stylesheet">

<style type="text/css">
	.bs-callout {
	    padding: 20px;
	    margin: 20px 0;
	    border: 1px solid #eee;
	    border-left-width: 5px;
	    border-radius: 3px;
	}
	.bs-callout h4 {
	    margin-top: 0;
	    margin-bottom: 5px;
	}
	.bs-callout p:last-child {
	    margin-bottom: 0;
	}
	.bs-callout code {
	    border-radius: 3px;
	}
	.bs-callout+.bs-callout {
	    margin-top: -5px;
	}
	.bs-callout-default {
	    border-left-color: #777;
	}
	.bs-callout-default h4 {
	    color: #777;
	}
	.bs-callout-primary {
	    border-left-color: #428bca;
	}
	.bs-callout-primary h4 {
	    color: #428bca;
	}
	.bs-callout-success {
	    border-left-color: #5cb85c;
	}
	.bs-callout-success h4 {
	    color: #5cb85c;
	}
	.bs-callout-danger {
	    border-left-color: #d9534f;
	}
	.bs-callout-danger h4 {
	    color: #d9534f;
	}
	.bs-callout-warning {
	    border-left-color: #f0ad4e;
	}
	.bs-callout-warning h4 {
	    color: #f0ad4e;
	}
	.bs-callout-info {
	    border-left-color: #5bc0de;
	}
	.bs-callout-info h4 {
	    color: #5bc0de;
	}
</style>