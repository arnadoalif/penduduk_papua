<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');

		// Load Model
		$this->load->model('Login_model');
	}

	public function index()
	{
		$this->load->view('login/login_view');
	}

	public function action_login() {

		$m_login = new Login_model();

		$username  = $this->input->post('username');
		$password  = $this->input->post('password');

		$result = $m_login->check_login('dt_login', $username, $password);
		if ($result > 0) {
			$update_log = $m_login->update_data_log('dt_login', 'login', $username, $password);
			if ($update_log > 0) {
				redirect(base_url(),'refresh');
			} else {
				redirect('login','refresh');
			}
		} else {
			redirect('login','refresh');
		}
	}

	public function action_logout() {
		$m_login = new Login_model();
		$user = $this->session->userdata('user');
		$password = $this->session->userdata('password');

		$update_log = $m_login->update_data_log('dt_login', 'logout', $user,$password);
		if ($update_log > 0) {
			redirect('login','refresh');
		} else {
			redirect(base_url(),'refresh');
		}
	}

}

/* End of file Login_view.php */
/* Location: ./application/controllers/Login_view.php */