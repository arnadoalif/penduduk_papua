<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if($this->session->userdata('Session_log') != 1){
			redirect('login');
		}
		
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
		
		/* Load Model */
		$this->load->model('Penduduk_model');
	}

	public function index()
	{
		$this->load->view('adminpages/page/home_view');
	}

	/**
	
		TODO:
		- Front System 
	    - Alif
	 */
	
	public function data_penduduk_view() {
		$m_penduduk = new Penduduk_model();
		$data['data_penduduk'] = $m_penduduk->get_data_penduduk('dt_penduduk')->result();

		$this->load->view('adminpages/page/data_penduduk_view', $data);
	}

	public function data_penduduk_distrik() {
		$m_penduduk = new Penduduk_model();
		$data['data_kec'] = $m_penduduk->get_data_penduduk_distrik('dt_penduduk_excel', 'kec')->result();
		$data['data_kampung'] = $m_penduduk->get_data_penduduk_distrik('dt_penduduk_excel', 'kampung')->result();

		$this->load->view('adminpages/page/data_menu_distrik_view', $data);
	}

	public function get_data_filter_distik() {
		$m_penduduk = new Penduduk_model();

		$data_distrik = $this->input->post('data_1');
		$data_kampung = $this->input->post('data_2');

		$data['data_penduduk'] = $m_penduduk->get_data_filter_distik('tbl_penduduk_excel', $data_distrik, $data_kampung)->result();
		$this->load->view('adminpages/page/get_data_distrik_view', $data);
	}

	public function get_data_filter_kampung() {
		$m_penduduk = new Penduduk_model();

		$data_kampung = $this->input->post('data_2');
		$data['data_penduduk'] = $m_penduduk->get_data_filter_kampung('tbl_penduduk_excel', $data_kampung)->result();
		$this->load->view('adminpages/page/get_data_distrik_view', $data);
	}

	public function data_penduduk_kampung() {
		$m_penduduk = new Penduduk_model();
		$data['data_kampung'] = $m_penduduk->get_data_penduduk_distrik('dt_penduduk_excel', 'kampung')->result();

		$this->load->view('adminpages/page/data_menu_kampung_view', $data);
	}

	public function tambah_penduduk_view() {
		$this->load->view('adminpages/page/tambah_data_penduduk_view');
	}

	public function edit_penduduk_view($no_kk, $no_nik) {
		$m_penduduk = new Penduduk_model();
		$data['data_penduduk'] = $m_penduduk->get_data_penduduk_single_data('dt_penduduk', $no_kk, $no_nik)->row();
		$this->load->view('adminpages/page/edit_data_penduduk_view', $data);
	}


	/**
	==============================================================
		TODO:
		- Front System
		- Pakhuda
	
	 */
	
	

	/**
	==============================================================
		TODO:
		- Backend System
		- Alif
	
	 */
	
	public function action_tambah_data_penduduk() {
		$m_penduduk = new Penduduk_model();
	    $data = array(
	    	'kk'			  => $this->input->post('kk'), 
		    'nik'			  => $this->input->post('nik'), 
		    'kelurahan'		  => $this->input->post('kelurahan'), 
		    'nama'			  => strtoupper($this->input->post('nama')), 
		    'kelamin'		  => $this->input->post('kelamin'), 
		    'tempat_lahir'		  => strtoupper($this->input->post('tempat')), 
		    'tanggal_lahir'   => $this->input->post('tanggal_lahir'), 
		    'shdk'			  => $this->input->post('shdk'), 
		    'shdrt'			  => $this->input->post('shdrt'), 
		    'darah'  => $this->input->post('golongan_darah'), 
		    'agama'			  => $this->input->post('agama'), 
		    'status'		  => $this->input->post('status'), 
		    'pendidikan'	  => $this->input->post('pendidikan'), 
		    'pekerjaan'		  => strtoupper($this->input->post('pekerjaan')), 
		    'namaibu'		  => strtoupper($this->input->post('nama_ibu')), 
		    'nama_ayah'		  => strtoupper($this->input->post('nama_ayah')), 
		    'kepala_keluarga' => strtoupper($this->input->post('kepala_keluarga')), 
		    'alamat'		  => strtoupper($this->input->post('alamat')), 
		    'prop'			  => strtoupper($this->input->post('prop')), 
		    'kab'			  => strtoupper($this->input->post('kab')), 
		    'kec'			  => strtoupper($this->input->post('kec')), 
		    'kampung'		  => strtoupper($this->input->post('kampung')),
		    'waktu_input'		  => date("Y-m-d H:i:s"),
		    'dihapus'		  => 'TIDAK'
	    ); 

	    $m_penduduk->insert_data_penduduk('dt_penduduk', $data);
		$this->session->set_flashdata('message_data', '<strong>Success </strong> Penduduk baru berhasil di tambahakan.');
		redirect($this->agent->referrer());
	}

	public function action_edit_data_penduduk() {
		$m_penduduk = new Penduduk_model();
		$old_kk = $this->input->post('old_kk');
		$old_nik = $this->input->post('old_nik');

	    $data = array(
	    	'kk'			  => $this->input->post('kk'), 
		    'nik'			  => $this->input->post('nik'), 
		    'kelurahan'		  => $this->input->post('kelurahan'), 
		    'nama'			  => strtoupper($this->input->post('nama')), 
		    'kelamin'		  => $this->input->post('kelamin'), 
		    'tempat_lahir'		  => strtoupper($this->input->post('tempat')), 
		    'tanggal_lahir'   => $this->input->post('tanggal_lahir'), 
		    'shdk'			  => $this->input->post('shdk'), 
		    'shdrt'			  => $this->input->post('shdrt'), 
		    'darah'  => $this->input->post('golongan_darah'), 
		    'agama'			  => $this->input->post('agama'), 
		    'status'		  => $this->input->post('status'), 
		    'pendidikan'	  => $this->input->post('pendidikan'), 
		    'pekerjaan'		  => strtoupper($this->input->post('pekerjaan')), 
		    'namaibu'		  => strtoupper($this->input->post('nama_ibu')), 
		    'nama_ayah'		  => strtoupper($this->input->post('nama_ayah')), 
		    'kepala_keluarga' => strtoupper($this->input->post('kepala_keluarga')), 
		    'alamat'		  => strtoupper($this->input->post('alamat')), 
		    'prop'			  => strtoupper($this->input->post('prop')), 
		    'kab'			  => strtoupper($this->input->post('kab')), 
		    'kec'			  => strtoupper($this->input->post('kec')), 
		    'kampung'		  => strtoupper($this->input->post('kampung'))
	    ); 

	    $result = $m_penduduk->update_data_penduduk('dt_penduduk','kk','nik', $old_kk, $old_nik, $data);
	    if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Data penduduk berhasil di ubah.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat mengubah data penduduk');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete() {
		$m_penduduk = new Penduduk_model();

		$kk = $this->input->post('kk');
		$nik = $this->input->post('nik');
		$data = array('dihapus' => 'YA');
		$result = $m_penduduk->delete_data_penduduk('dt_penduduk', $kk, $nik, $data);
		if ($result > 0) {
			$this->session->set_flashdata('message_data', '<strong>Success </strong> Data penduduk berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Upss!! </strong> Kesalahan saat menghapus data penduduk');
			redirect($this->agent->referrer());
		}
	}


	/**
	==============================================
		TODO:
		- Backend System
		- Pak Huda
	
	 */
	
	

}

/* End of file Administrator.php */
/* Location: ./application/controllers/Administrator.php */