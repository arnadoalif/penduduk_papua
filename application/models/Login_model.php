<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function check_login($table_name, $user, $password) {

		$valid = $this->db->where('user', $user);
		$valid = $this->db->where('password', $password);
		$valid = $this->db->get($table_name);
		if ($valid->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function update_data_log($table_name, $key_log, $user, $password) {
		$this->db->where('user', $user);
		$this->db->where('password', $password);
		$result = $this->db->get($table_name);

		if ($key_log == "login") {
			$data = array(
				'log_time' => date("Y-m-d H:i:s")
			);
			$this->db->where('user', $user);
			$this->db->where('password', $password);
			$this->db->update($table_name, $data);

			$data_session = array(
				'kode_login' => $result->row('kode_login'),
				'user' => $result->row('user'),
				'Session_log' => 1
			);
			$this->session->set_userdata($data_session);

			return true;
		} else if ($key_log == "logout") {
			$data = array(
				'Out_time' => date("Y-m-d H:i:s")
			);
			$this->db->where('user', $user);
			$this->db->where('password', $password);
			$this->db->update($table_name, $data);

			$this->session->unset_userdata(array('kode_login'=> '', 'user' => '', 'Session_log' => ''));
			session_destroy();
			return;
		} else {
			return false;
		}
	}	

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */