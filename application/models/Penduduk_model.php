<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk_model extends CI_Model {

	public function get_data_penduduk($table_name) {
		$this->db->select('*');
		$this->db->where('dihapus', 'TIDAK');
		return $this->db->get($table_name);
	}

	public function get_data_penduduk_single_data($table_name, $kode_kk, $kode_nik) {
		$sql = "select * from $table_name where kk='$kode_kk' and nik='$kode_nik' limit 1";
		return $this->db->query($sql);
	}

	public function get_data_filter_distik($table_name, $data_distrik, $data_kampung) {
		$sql = "SELECT * FROM dt_penduduk_excel WHERE kec LIKE '%$data_distrik%' AND kampung LIKE '%$data_kampung%'";
		return $this->db->query($sql);
	}

	public function get_data_filter_kampung($table_name, $data_kampung) {
		$sql = "SELECT * FROM dt_penduduk_excel WHERE kampung LIKE '%$data_kampung%'";
		return $this->db->query($sql);
	}

	public function get_data_penduduk_distrik($table_name, $field) {
		$sql = "SELECT DISTINCT $field FROM $table_name";
		return $this->db->query($sql);
	}

	public function insert_data_penduduk_valid($table_name, $row_kk, $row_nik, $data_kk, $data_nik, $data) {
		$result = $this->db->where($row_kk, $data_kk);
		$result = $this->db->where($row_nik, $data_nik);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			return false;
		} else {
			$this->db->insert($table_name, $data);
			return true;
		}
	}

	public function insert_data_penduduk($table_name, $data) {
		$this->db->insert($table_name, $data);
	}

	public function update_data_penduduk($table_name, $row_kk, $row_nik, $kode_kk, $kode_nik, $data) {
		$result = $this->db->where($row_kk, $kode_kk);
		$result = $this->db->where($row_nik, $kode_nik);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where($row_kk, $kode_kk);
			$this->db->where($row_nik, $kode_nik);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
			
		}
	}

	public function delete_data_penduduk($table_name, $kk, $nik, $data) {
		$result = $this->db->where('kk', $kk);
		$result = $this->db->where('nik', $nik);
		$result = $this->db->get($table_name, 1);
		if ($result->num_rows() > 0) {
			$this->db->where('kk', $kk);
			$this->db->where('nik', $nik);
			$this->db->update($table_name, $data);
			return true;
		} else {
			return false;
		}
	}

}

/* End of file Penduduk_model.php */
/* Location: ./application/models/Penduduk_model.php */